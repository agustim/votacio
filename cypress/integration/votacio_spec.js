
describe("Voting Client", () => {
  it("Load Page", () => {
    cy.visit("/")
    cy.title().should('include', 'Votacions')
  })
  it("View modal", () => {
    cy.visit("/")
    cy.get('#modal-confirm-blanks').should('to.be.hidden')
    cy.get('form > .btn-primary').click()
    cy.get('#modal-confirm-blanks').should('to.be.visible')
    cy.get('.close').click()
    cy.get('#modal-confirm-blanks').should('to.be.hidden')
  })
})
