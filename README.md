# frontend

> Voting project frontend

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Testing

``` bash
# install dependencies
npm install

# run Testing
npm run test

```

## Open cypress

Sometimes when you open cypress, you receive whatch ENOSPC error. This means that you need to increase fs.inotify.max_user_watches.  

You can execute this sentence to fix it.

``` bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```
