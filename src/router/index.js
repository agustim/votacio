import Vue from 'vue'
import Router from 'vue-router'
import Questionnaire from '@/components/Questionnaire'
import Default from '@/components/Default'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/:hash/:seed',
      name: 'Questionnaire',
      component: Questionnaire
    },
    {
      path: '/',
      name: 'Default',
      component: Default
    }
  ]
})
